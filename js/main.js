llamandoFetch =()=>{
    const url = 'https://jsonplaceholder.typicode.com/todos';
    fetch(url)
    .then(response => response.json())
    .then(data => mostrarTodos(data))
    .catch((reject)=>{
        console.log("surgio un error"+reject)
    });

}


const res = document.getElementById('respuesta');
const mostrarTodos=(data)=>{
     res.innerHTML="";

    for(let item of data){
        res.innerHTML+="<span>"+item.userId+","+item.id+","+item.title+","+item.completed+"</span><br><br>";
        
    }
}

const llamandoAwait = async () =>{
    try{
        const url="https://jsonplaceholder.typicode.com/todos";
        const respuesta= await fetch(url);
        const data = await respuesta.json();
        mostrarTodos(data);
    }catch(error){
        console.log("surgio un error"+error);
    }
}





document.getElementById("btnCargarP").addEventListener('click',function(){
    llamandoFetch();
});
document.getElementById("btnLimpiar").addEventListener('click',function(){
    res.innerHTML="";

});
document.getElementById("btnCargarA").addEventListener('click',function(){
    llamandoAwait();

})
