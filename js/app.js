function peticion(){
    
    var http = new XMLHttpRequest();
    
    http.open('GET', 'https://jsonplaceholder.typicode.com/albums', true);
    
    // Configurar el manejo de eventos para la respuesta
    http.onreadystatechange = function() {
        // Verificar si la solicitud se completó satisfactoriamente
        if (http.readyState == 4 && http.status == 200) {
           
            // Manejar la respuesta
            var responseData = JSON.parse(http.responseText);
            console.log(responseData);
            var tbody = document.querySelector('#lista');
            tbody.innerHTML="";
            for(const datos of responseData){
                tbody.innerHTML += '<tr><th class=columna1>'+datos.userId+'</th>'+'<th class="columna2">'+datos.id+'</th>'+'<th class="columna3">'+datos.title+'</th></tr>'
            }
        }
    };
    
    // Enviar la solicitud
    http.send();
    }
    
    var btncargar = document.querySelector('#btncargar');
    var btnLimpiar = document.querySelector('#btnLimpiar');
    
    btncargar.addEventListener('click',function(){
        peticion();
    });
    
    btnLimpiar.addEventListener('click',function(){
        document.querySelector('#lista').innerHTML="";
    });
    
    

    
    