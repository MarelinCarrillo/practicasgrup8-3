document.addEventListener('DOMContentLoaded', () => {
    const breedsSelect = document.getElementById('breeds');
    const dogImage = document.getElementById('dogImage');
    const loadBreedButton = document.getElementById('loadBreedButton');
    const showImageButton = document.getElementById('showImageButton');

    let selectedBreed = '';

    // Obtener la lista de razas de perros al cargar la página
    fetch('https://dog.ceo/api/breeds/list')
        .then(response => response.json())
        .then(data => {
            const breeds = data.message;
            populateBreedsSelect(breeds);
        })
        .catch(error => console.error('Error fetching breeds:', error));

    // Llenar el select con las razas de perros
    function populateBreedsSelect(breeds) {
        breeds.forEach(breed => {
            const option = document.createElement('option');
            option.value = breed;
            option.textContent = breed;
            breedsSelect.appendChild(option);
        });
    }

    // Cargar razas al hacer clic en el botón
    loadBreedButton.addEventListener('click', () => {
        selectedBreed = breedsSelect.value;
    });

    // Ver imagen al hacer clic en el botón
    showImageButton.addEventListener('click', () => {
        if (selectedBreed) {
            fetchDogImage(selectedBreed);
        } else {
            alert('Primero carga una raza antes de ver la imagen.');
        }
    });

    // Obtener y mostrar una imagen aleatoria de la raza seleccionada
    function fetchDogImage(breed) {
        const imageUrl = `https://dog.ceo/api/breed/${breed}/images/random`;

        fetch(imageUrl)
            .then(response => response.json())
            .then(data => {
                const imageUrl = data.message;
                dogImage.src = imageUrl;
            })
            .catch(error => console.error('Error fetching dog image:', error));
    }
});
